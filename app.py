from flask import Flask, render_template, redirect, url_for
from celery import Celery
import mysql.connector
from flask import request
import json
import requests
from bs4 import BeautifulSoup

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'amqp://hershil:hershil123@localhost/hershil_vhost'
app.config['CELERY_RESULT_BACKEND'] = 'rpc://'
celery = Celery('tasks', broker='amqp://hershil:hershil123@localhost/hershil_vhost', backend='rpc://')
# celery.conf.update(app.config)





#Sets up database connection
def dbConnection():
    dbHost = 'localhost'
    dbUser = 'Hershil'
    dbPassword = 'H3r5h!l007'
    db = 'celery_test'

    mydb = mysql.connector.connect(host=dbHost, user=dbUser, password=dbPassword, database=db)
    if(mydb):
        return mydb
    else:
        return None

#Used for executing SQL statements
def selectStatement(dbConnection, sqlQuery):
    dbCursor = dbConnection.cursor(buffered=True)
    dbCursor.execute(sqlQuery)
    return dbCursor

def insertStatement(dbConnection, sqlQuery, values):
    dbCursor = dbConnection.cursor(buffered=True)
    dbCursor.execute(sqlQuery, values)
    return dbCursor

def get_movie_info(titleID):

    data = json.loads(titleID)

    title_ID = data["titleID"]

    target_url = 'https://www.imdb.com/title/' + title_ID

    page = requests.get(target_url).text

    soup = BeautifulSoup(page, 'html.parser')

    poster_box = soup.find('div', {'class': 'poster'})

    poster_img = list(poster_box.children)[1]

    poster_tag = list(poster_img.children)[1]

    poster_link = poster_tag['src']

    plot_box = soup.find('div', {'class': 'summary_text'})

    plot_text = list(plot_box.children)[0]

    plot_text = plot_text.lstrip()

    info = {
        "posterLink": poster_link,
        "plotText": plot_text
    }

    return json.dumps(info)




#Search Page route
@app.route('/search_page', methods=["GET", "POST"])
def search_page():
    data = None
    if request.method == "POST":
        search_criteria = request.form.get('search_criteria', False)

        info = {
            "search": search_criteria
        }

        data = search_movie(json.dumps(info))

        if(data == {}):
            error = "No movies of that title exist!"
            state = {
                "status": error
            }
            return json.dumps(state)
        else:
            return redirect(url_for("search_results_page", data=data))
    return render_template("search_page.html", data=data)

@app.route('/search_results_page', methods=["GET", "POST"])
def search_results_page():
    data = request.values.get('data')
    loaded_data = json.loads(data)
    #load.delay(loaded_data)

    for i in loaded_data:
        title_ID = loaded_data[i]["movieID"]

        target_url = 'https://www.imdb.com/title/' + title_ID

        page = requests.get(target_url).text

        soup = BeautifulSoup(page, 'html.parser')

        poster_box = soup.find('div', {'class': 'poster'})

        poster_img = list(poster_box.children)[1]

        poster_tag = list(poster_img.children)[1]

        poster_link = poster_tag['src']

        plot_box = soup.find('div', {'class': 'summary_text'})

        plot_text = list(plot_box.children)[0]

        plot_text = plot_text.lstrip()


        myDb = dbConnection()
        print(myDb)
        sqlString = "INSERT INTO scraped_data (posterLink, plotDesc, tconst) VALUES (%s, %s, %s)"
        values = (poster_link, plot_text, title_ID)
        result = insertStatement(myDb, sqlString, values)
        myDb.commit()

    return render_template("search_results_page.html", data=loaded_data)

#Route for movie details page
@app.route('/movie_details_page', methods=["GET", "POST"])
def movie_details_page():
    movie_title = request.values.get('title')
    movieID = request.values.get('id')

    info = {
        "titleID": movieID
    }


    myDb = dbConnection()
    print(myDb)
    sqlString = "SELECT posterLink, plotDesc FROM scraped_data WHERE tconst = " + "'" + movieID + "'"
    result = selectStatement(myDb, sqlString)
    fetch = result.fetchone()

    poster_link = fetch[0]
    plot_text = fetch[1]

    # scraped_data = get_movie_info(json.dumps(info))
    # loaded_scraped_data = json.loads(scraped_data)
    # poster_link = loaded_scraped_data["posterLink"]
    # plot_text = loaded_scraped_data["plotText"]

    return render_template('movie_details_page.html', movie_title=movie_title, poster_link=poster_link, plot_text=plot_text)


#Function for getting movies from database based on search criteria.
def search_movie(search_criteria):
    data = json.loads(search_criteria)
    search = data["search"]

    myDb = dbConnection()
    print(myDb)
    sqlString = "SELECT tconst, primaryTitle FROM titles WHERE primaryTitle LIKE " + "'" + search + "%'" + "AND titleType = 'movie'"
    result = selectStatement(myDb, sqlString)
    fetch = result.fetchall()
    info = {}
    for row in fetch:
        info[row[0]] = {
            "movieID": row[0],
            "title": row[1],
        }

    return json.dumps(info)

@app.route('/test', methods=['GET','POST'])
def test():
    add.delay(5, 6)
    return 'done'

@celery.task(name='test_celery.add')
def add(x, y):
    return x + y

@celery.task(name='test_celery.load')
def load(data):
    for i in data:
        title_ID = data[i]["movieID"]

        target_url = 'https://www.imdb.com/title/' + title_ID

        page = requests.get(target_url).text

        soup = BeautifulSoup(page, 'html.parser')

        poster_box = soup.find('div', {'class': 'poster'})

        poster_img = list(poster_box.children)[1]

        poster_tag = list(poster_img.children)[1]

        poster_link = poster_tag['src']

        plot_box = soup.find('div', {'class': 'summary_text'})

        plot_text = list(plot_box.children)[0]

        plot_text = plot_text.lstrip()


        myDb = dbConnection()
        print(myDb)
        sqlString = "INSERT INTO scraped_data (posterLink, plotDesc, tconst) VALUES (%s, %s, %s)"
        values = (poster_link, plot_text, title_ID)
        result = insertStatement(myDb, sqlString, values)
        myDb.commit()
    return 'finished'

if __name__ == '__main__':
    app.run(debug=True)
